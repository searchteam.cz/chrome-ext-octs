# Candidate Tracking System (CTS) Toolset (Chrome extension built with Webpack + TypeScript + React)

IMPORTANT: This extension is intended to be used with Candidate Tracking System owned by SearchTeam s.r.o. and it will not work without it.

Make your life easier, this extension will help you to work with your candidate tracking system (CTS).

Everytime you visit candidate or search candidate  on LinkedIn, extension will provide you match with additional informations about candidate from your company CTS.
In your CTA on candidate page you can hit one button and open multiple tabs with various searches about the candidate. Searches are optimized and defined by company policy in CTS.
Stay secure, if you forgot to logout or your screensaver fail to show up, extension will log you out from all google services after time defined by company policy in CTS.

IMPORTANT: This extension is intended to be used with Candidate Tracking System owned by SearchTeam s.r.o. and it will not work without it.


## Building

1.  Clone repo
2.  `npm i`
3.  `npm run watch` to run in watch mode
4.  `npm run build` to build a production (minified) version

## Installation

1.  Complete the steps to build the project above
2.  Go to [_chrome://extensions_](chrome://extensions) in Google Chrome
3.  With the developer mode checkbox ticked, click **Load unpacked extension...** and select the _dist_ folder from this repo
