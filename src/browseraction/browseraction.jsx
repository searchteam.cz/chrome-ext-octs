import * as React from 'react';
import * as ReactDOM from 'react-dom';
import styled, { createGlobalStyle, css } from 'styled-components';
import 'typeface-roboto';
import { Button , CssBaseline, Paper, Tabs, Tab } from '@material-ui/core';
import { AddCircleSharp as AddCircleSharpIcon, RemoveCircleSharp as RemoveCircleSharpIcon, Favorite as FavoriteIcon } from '@material-ui/icons';
import { StylesProvider } from "@material-ui/styles";

/**
    import ./global.css
    ----------------------------------
    @import "normalize/opinionated";
    @import "sanitize/*";

    html, body {
        height: 100%;
        width: 100%;
        font-family: 'Roboto', sans-serif;
    }

    html, body {
        width: 200px;
        padding: 0;
    }
    
    svg
        //https://stackoverflow.com/questions/24626908/how-to-get-rid-of-extra-space-below-svg-in-div-element
        vertical-align: top
    ----------------------------------
*/

const GlobalStyle = createGlobalStyle`
`

const Menu = styled.div`,
`;

interface Props {

}

interface State {
    value: number | null
}

const StyledPaper = styled(Paper)`
    background: red;
`;

class Root extends React.Component<Props, State> {

    constructor(props : Props) {
        super(props);
        this.state = {value: null};        
      }

    handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {        
        this.setState(state => ({
            value: newValue
        }));
    }

    render() {
        return (
            
            <React.Fragment>
                <CssBaseline />
                <GlobalStyle />
                <StyledPaper square elevation={3}>
                    <Tabs    
                        value={this.state.value}
                        onChange={this.handleChange}
                        variant="fullWidth"
                        indicatorColor="secondary"
                        textColor="secondary"
                        aria-label="icon label tabs example"
                    >
                        <Tab icon={<AddCircleSharpIcon />} label="Add" />                        
                        <Tab icon={<RemoveCircleSharpIcon />} label="Remove" />
                    </Tabs>
                </StyledPaper>
            </React.Fragment>
            
        )
    }
}

ReactDOM.render(<Root />, document.querySelector("#root"));

