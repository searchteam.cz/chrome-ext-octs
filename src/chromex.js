
export class Chromex {

    postMessage = (data) => new Promise((res, rej) => {
        chrome.runtime.sendMessage(data, function(response) {
            if(response) {
                res(response);
            } else {
                rej();
            }
        });
    })

}

export default new Chromex();
