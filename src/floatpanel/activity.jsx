import * as React from 'react';
import {
    Grid as DefaultGrid,
    Box as DefaultBox,
    Typography as DefaultTypography
} from '@material-ui/core';
import Moment from 'react-moment';
import styled from 'styled-components';



const Text = styled(DefaultTypography)`
    &.primary, &.secondary {
        font-size: 1.4rem;
        font-weight: 500;
    }

    &.tertiary {
        font-size: 1.4rem;
        font-weight: 400;
    }
`;

const Grid = styled(DefaultGrid)``;
const Box = styled(DefaultBox)`
    padding: ${props => props.theme.spacing(0.5)}px 0px;
`;


export class Activity extends React.Component {

    constructor(props) {
        super(props);

        var tmp = document.createElement("DIV");
        tmp.innerHTML = props.activity.notes;
        props.activity.notes = tmp.textContent || tmp.innerText || "";

    }

    render() {
        return (
            <Box className={this.props.className}>
                <Grid container direction="column" className="container" >
                    <Grid item >
                        <Text className="primary" display="inline"><Moment format="DD-MM-YYYY">{this.props.activity.dateCreated}</Moment></Text>&nbsp;-&nbsp;<Text className="secondary" display="inline">{this.props.activity.typeDescription}</Text>
                    </Grid>
                    <Grid item>
                        <Text className="tertiary">{this.props.activity.notes}</Text>
                    </Grid>
                </Grid>

            </Box>
        )
    }
}




