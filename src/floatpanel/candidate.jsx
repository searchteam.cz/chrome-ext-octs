import * as React from 'react';
import {
    Grid as DefaultGrid,
    Switch as DefaultSwitch,
    Divider as DefaultDivider
} from '@material-ui/core';
import { Header as DefaultHeader } from './header';
import { Keyskill as DefaultKeyskill } from './keyskill';
import { Activity as DefaultActivity } from './activity';
import { Pipeline as DefaultPipeline } from './pipeline';
import { Section as DefaultSection } from './section';

import styled from 'styled-components';



const Header = styled(DefaultHeader)``
const Activity = styled(DefaultActivity)``
const Keyskill = styled(DefaultKeyskill)``
const Pipeline = styled(DefaultPipeline)``
const Section = styled(DefaultSection)``;
const Divider = styled(DefaultDivider)``;
const Switch = styled(DefaultSwitch)``;


const Grid = styled(DefaultGrid)`
    & .workaround {
        text-align: right;
    }

    & .grey {
        background: ${props => props.theme.palette.divider};
    }
`;

export class Candidate extends React.Component {
    constructor(props) {
        super(props);
        this.state = {checked: true, keyskill: true, pipeline: true, activity: false};
    }

    handler = () => {
        this.setState(state => ({
            checked: !state.checked
        }));
    }

    handle = (key) => {
        this.setState(state => ({[key]: !state[key]}));
        chrome.storage.sync.set({'state' : this.state});
    }

    componentDidMount = () => {
        chrome.storage.sync.get(['state'], (result) => {
            this.setState(result.state);
        });
    }

    render() {
        let candidate = this.props.candidate;

        return (
            <Grid key={candidate.candidateId} container direction="column">
                <Grid item>
                    <Header className="handle" candidate={candidate} action={this.handler} checked={this.state.checked} />
                </Grid>
                {this.state.checked &&
                <React.Fragment>
                    <Grid container item direction="row" wrap="nowrap" justify="space-between">
                        <Grid item xs={10}>
                        {candidate.keyskills && this.state.keyskill &&
                            <Section>
                                {candidate.keyskills.map((keyskill) =>
                                    <Keyskill key={keyskill.id} keyskill={keyskill} />
                                )}
                            </Section>
                        }
                        </Grid>
                        <Grid className="workaround" item xs={2}>
                            <Switch checked={this.state.keyskill} onClick={() => this.handle('keyskill')} color="default" size="small" />
                        </Grid>
                    </Grid>
                    <Grid className="grey" container item direction="row" wrap="nowrap" justify="space-between">
                        <Grid item xs={10}>
                        {candidate.pipelines && this.state.pipeline &&
                            <Section>
                                {candidate.pipelines.map((pipeline) =>
                                    <Pipeline key={pipeline.candidateJoborderId} pipeline={pipeline} />
                                )}
                            </Section>
                        }
                        </Grid>
                        <Grid className="workaround" item xs={2}>
                            <Switch checked={this.state.pipeline} onClick={() => this.handle('pipeline')} color="default" size="small" />
                        </Grid>
                    </Grid>
                    <Grid  container item direction="row" wrap="nowrap" >
                        <Grid item xs={10}>
                        {candidate.activities && this.state.activity &&
                            <Section>
                                {candidate.activities.slice(0,5).map((activity) => (
                                    <React.Fragment key={activity.activityID}>
                                        <Activity  activity={activity} />
                                        <Divider />
                                    </React.Fragment>
                                    )
                                )}
                            </Section>
                        }
                        </Grid>
                        <Grid className="workaround" item xs={2}>
                            <Switch checked={this.state.activity} onClick={() => this.handle('activity')} color="default" size="small" />
                        </Grid>
                    </Grid>
                </React.Fragment>
                }
            </Grid>
        )
    }
}



