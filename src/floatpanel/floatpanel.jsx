import * as React from 'react';
import Draggable from 'react-draggable';
import axios from 'axios';
import { RouteComponentProps } from 'react-router-dom';
import {
    CssBaseline,
    Paper as DefaultPaper,
} from '@material-ui/core';
import styled from 'styled-components';
import { createGlobalStyle } from 'styled-components';
import { Candidate as DefaultCandidate } from './candidate';

import linkedin from '../linkedin';
import chromex from '../chromex';

const GlobalStyle = createGlobalStyle``
const Candidate = styled(DefaultCandidate)``

const Paper = styled(DefaultPaper)`
    position: fixed;
    top: 48px;
    right: 0;
    margin: 8px;
    width: 368px;
    z-index: 1000;
`;



export class FloatPanel extends React.Component {

    constructor(props) {
        super(props);
        this.state = {candidates: []};
    }

    componentDidMount = () => {
        this.fetchCandidate();

        chrome.storage.sync.get(['defaultPosition'], (result) => {
            this.setState({ 'defaultPosition' : result.defaultPosition });
        });
    }

    componentDidUpdate = (prevProps) => {

        if (prevProps.match.params.id != this.props.match.params.id) {
            let c = [{
                candidateId : 0,
                profile : {
                    image : "",
                    firstName : "loading",
                    lastName : "loading"
                },
                employers: [{
                    name: 'loading'
                }],
                linkedin: {
                    profilePosition: [{
                        companyName : 'loading'
                    }]
                },
                match: {
                    publicIdentifier: 0
                }
            }];
            this.setState({candidates: c});

            this.fetchCandidate();
        }



    }

    fetchCandidate = async () => {




        let publicIdentifier = this.props.match.params.id;
        let profileLinkedin = await linkedin.getProfileByPublicIdentifier(publicIdentifier);
        let profileDatabaze = await chromex.postMessage({action: 'getCandidate', data: { profile: [{ publicIdentifier: publicIdentifier }] }});

        let candidates = [];




        if (profileDatabaze.data.length) {
            candidates = profileDatabaze.data;
        } else if (profileLinkedin.profile.length) {
            candidates = [{
                candidateId : 0,
                profile : {
                    image : profileLinkedin.profile[0].image,
                    firstName : profileLinkedin.profile[0].firstName,
                    lastName : profileLinkedin.profile[0].lastName
                },
                employers: [{
                    name: '-'
                }],
                profilePosition: [{
                    companyName : '-'
                }],
                match: {
                    publicIdentifier: 0
                }
            }];
        }

        candidates = candidates.map((candidate) => {
            candidate.linkedin = profileLinkedin;
            return candidate;
        });

        this.setState({candidates: candidates});
    }

    handleStop = (e, data) => {
        this.setState({ 'defaultPosition' : { x: data.x, y: data.y } });
        chrome.storage.sync.set({'defaultPosition' : { x: data.x, y: data.y }});
    }


    render() {
        return (
            <React.Fragment>
                <CssBaseline />
                <GlobalStyle />
                <Draggable
                    handle=".handle"
                    position={this.state.defaultPosition}
                    onStop={this.handleStop}
                    >
                    <Paper square elevation={3}>
                        {this.state.candidates.map((candidate) =>
                            <Candidate key={candidate.candidateId} candidate={candidate} />
                        )}
                    </Paper>
                </Draggable>
            </React.Fragment>
        )
    }
}