import * as React from 'react';
import {
    Avatar as DefaultAvatar,
    Typography as DefaultTypography,
    Grid as DefaultGrid,
    Box as DefaultBox,
    Switch as DefaultSwitch,
    Checkbox as DefaultCheckbox,
    IconButton as DefaultIconButton
} from '@material-ui/core';
import styled from 'styled-components';

import { LinkedIn as DefaultBusinessIcon, Favorite as DefaultFavoriteIcon, FavoriteBorder as DefaultFavoriteBorderIcon, Apartment as DefaultApartmentIcon, PersonAdd as DefaultPersonAddIcon, PersonAddDisabled as DefaultPersonAddDisabledIcon, PlaylistAdd as DefaultPlaylistAddIcon, Link as DefaultLink } from '@material-ui/icons';

const Text = styled(DefaultTypography)``;
const Grid = styled(DefaultGrid)``;
const Avatar = styled(DefaultAvatar)``;
const Switch = styled(DefaultSwitch)``;
const Checkbox = styled(DefaultCheckbox)``;
const Link = styled(DefaultLink)``;
const IconButton = styled(DefaultIconButton)``;
const BusinessIcon = styled(DefaultBusinessIcon)``;
const FavoriteIcon = styled(DefaultFavoriteIcon)``;
const FavoriteBorderIcon = styled(DefaultFavoriteBorderIcon)``;
const ApartmentIcon = styled(DefaultApartmentIcon)``;

const PersonAddIcon = styled(DefaultPersonAddIcon)``;
const PersonAddDisabledIcon = styled(DefaultPersonAddDisabledIcon)``;
const PlaylistAddIcon = styled(DefaultPlaylistAddIcon)``;


const Box = styled(DefaultBox)`
    cursor: default;
    padding: ${props => props.theme.spacing(0.5)}px;


    background: ${props => props.theme.palette.success.main};

    &.publicIdentifier {
        background: ${props => props.theme.palette.error.main};
    }

    &.keySkill, &.xLastDays {
        background: ${props => props.theme.palette.error.main};
    }

    & ${Text}, & ${BusinessIcon}, & ${ApartmentIcon} {
        color: ${props => props.theme.palette.primary.contrastText};
    }


    & ${Checkbox} {
        padding: 0;
    }

    & ${Text}.primary {
        text-transform: uppercase;
        font-weight: 500;
    }

    & ${Text}.secondary {
        font-size: 1.5rem;
    }

    & ${BusinessIcon}, & ${ApartmentIcon} {
        margin-right: ${props => props.theme.spacing(0.5)}px;
    }

    & ${Avatar} {
        width: ${props => props.theme.spacing(8)}px;
        height: ${props => props.theme.spacing(8)}px;
        border-radius: ${props => props.theme.spacing(0.5)}px;
        margin-right: ${props => props.theme.spacing(1)}px;
    }

    & svg {
        vertical-align: middle;
    }
`;


export class Header extends React.Component {

    render() {

        let className = Object.keys(this.props.candidate.match).filter(key => this.props.candidate.match[key]);
        className.push(this.props.className ? this.props.className : '');

        return (
            <Box className={className.join(' ')}>
                <Grid container direction="row" wrap="nowrap" alignItems="center">
                    <Grid item>
                        <Avatar imgProps={{'src': this.props.candidate.profile.image}} src="https://dev.searchteam.cz/images/favicon.png"/>
                    </Grid>
                    <Grid container item direction="column" wrap="nowrap">
                        <Grid item>
                            <Text className="primary">{this.props.candidate.profile.firstName} {this.props.candidate.profile.lastName}</Text>
                        </Grid>
                        <Grid container item direction="row" wrap="nowrap" >
                            <Grid item>
                                <ApartmentIcon fontSize="small" />
                            </Grid>
                            <Grid item>
                                <Text className="secondary">{this.props.candidate.employers[0].name}</Text>
                            </Grid>
                        </Grid>
                        <Grid container item direction="row" wrap="nowrap" >
                            <Grid item>
                                <BusinessIcon fontSize="small" />
                            </Grid>
                            <Grid item>
                                <Text className="secondary">{this.props.candidate.linkedin.profilePosition[0].companyName}</Text>
                            </Grid>
                        </Grid>

                    </Grid>
                    <Grid xs={3} container item direction="column" wrap="nowrap" alignItems="flex-end">
                        <Grid item>
                            <Switch checked={this.props.checked} onClick={this.props.action} color="default" size="small"/>
                        </Grid>
                        <Grid container item direction="row" wrap="nowrap" justify="flex-end" >
                            {!this.props.candidate.match['publicIdentifier']
                            ?
                            <React.Fragment>
                                <Grid item>
                                    <IconButton size="small" >
                                        <PersonAddIcon fontSize="small" />
                                    </IconButton>
                                </Grid>
                            </React.Fragment>
                            :
                            <React.Fragment>
                                <Grid item>
                                    <IconButton size="small" >
                                        <PlaylistAddIcon fontSize="small" />
                                    </IconButton>
                                </Grid>
                                <Grid item>
                                    <IconButton size="small" >
                                        <Link fontSize="small" />
                                    </IconButton>
                                </Grid>
                            </React.Fragment>
                            }
                        </Grid>
                    </Grid>
                </Grid>
            </Box>
        )
    }
}