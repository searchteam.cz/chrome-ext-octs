import * as React from 'react';
import {
    Chip as DefaultChip
} from '@material-ui/core';

import styled from 'styled-components';


const Chip = styled(DefaultChip)`
    height: ${props => props.theme.spacing(3)}px;
    margin: ${props => props.theme.spacing(0.25)}px;
    padding: ${props => props.theme.spacing(0.25)}px;
    border-radius: ${props => props.theme.spacing(0.5)}px;
    text-transform: uppercase;
    background: white;
    border: 1px solid rgba(0, 0, 0, 0.23);
`;


export class Keyskill extends React.Component {
    render() {
        return (
            <Chip className={this.props.className} label={this.props.keyskill.name} />
        )
    }
}

