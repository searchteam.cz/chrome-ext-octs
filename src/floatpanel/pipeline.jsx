import * as React from 'react';
import {
    Grid as DefaultGrid,
    Box as DefaultBox,
    Typography as DefaultTypography,
    Chip as DefaultChip,
    Paper as DefaultPaper
} from '@material-ui/core';

import styled from 'styled-components';

const Text = styled(DefaultTypography)``;
const Chip = styled(DefaultChip)`
    width: 150px;
`;

const Grid = styled(DefaultGrid)`
    &.item {
        white-space: nowrap;
        width: 300px;
        overflow: hidden;
        text-overflow: ellipsis;
    }

`;

const Box = styled(DefaultBox)``

const Paper = styled(DefaultPaper)`
    padding: 0.75rem;
    margin: 0.25rem;
    background: white;

    & ${Text}.primary {
        font-size: 1.4rem;
        font-weight: 500;
    }

    & ${Text}.secondary {
        font-size: 1.2rem;
        font-weight: 400;
    }
`;



export class Pipeline extends React.Component {
    render() {
        return (
            <Paper variant="outlined" className={this.props.className}>
                <Grid container direction="column" wrap="nowrap" >
                    <Grid container item direction="row" justify="space-between">
                        <Grid item >
                            <Text className="primary" display="inline">{this.props.pipeline.companyName}</Text>
                        </Grid>
                        <Grid item>
                            <Chip size="small" label={this.props.pipeline.status} />
                        </Grid>
                    </Grid>
                    <Grid item className="item">
                        <Text className="secondary" display="inline" >{this.props.pipeline.title}</Text>
                    </Grid>
                </Grid>
            </Paper>
        )
    }
}
