import * as React from 'react';
import {
    Grid as DefaultGrid
} from '@material-ui/core';

import styled from 'styled-components';

const GridContainer = styled(DefaultGrid)`
    /*border: 1px dashed ${props => props.theme.palette.primary.main};*/
    border-left: 0;
    border-right: 0;
    padding: ${props => props.theme.spacing(1)}px ${props => props.theme.spacing(0.5)}px;
`;
const GridItem = styled(DefaultGrid)``;


export class Section extends React.Component {
    render() {
        return (
            <GridContainer container className={this.props.className}>
                <GridItem item>
                    {this.props.children}
                </GridItem>
            </GridContainer>
        )
    }
}


