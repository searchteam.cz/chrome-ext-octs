/** TODO 16/01/2020: create simple component */
/** TODO 16/01/2020: consider https://github.com/mui-org/material-ui/blob/master/docs/src/modules/components/DemoSandboxed.js*/
/** TODO 16/01/2020: https://cssinjs.org/jss-api/?v=v10.0.3 */
/** TODO 16/01/2020: this is workaround, consider fork repository,
 * another possible workaround ("esModuleInterop": true in tsconfig.json),
 * it has to do something with exporting function in imported module 'default' */


import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {
    Router as Router,
    Switch,
    Route
} from "react-router-dom";
import { create } from "jss";
import { createBrowserHistory } from 'history';
import {
    createMuiTheme,
    ThemeProvider as MuiThemeProvider
} from '@material-ui/core/styles';
import {
    jssPreset,
    StylesProvider
} from "@material-ui/styles";
import {
    StyleSheetManager,
    ThemeProvider
} from 'styled-components';
import { FloatPanel } from "./floatpanel";


const shadow = document.body.appendChild(document.createElement("div")).attachShadow({ mode: "open" });
const html = shadow.appendChild(document.createElement("html"));
const header = html.appendChild(document.createElement("head"));
const body = html.appendChild(document.createElement("body"));
const root = body.appendChild(document.createElement("div"));

/** jss inserts style as sibling not as child */
const workaround = document.createElement("div");

const cjss = create({ ...jssPreset(), insertionPoint: header.appendChild(workaround) });
const history = createBrowserHistory();

import 'typeface-roboto';

const theme = createMuiTheme({
    typography: {
        htmlFontSize: 10,
        fontSize: 14
    },
    palette: {
        primary: {
            main: '#1c668d',
            contrastText: '#ffffff'
        },
        secondary: {
            main: '#8D431C',
            contrastText: '#ffffff'
        }
    }
});


/** TODO 22/01/2020: consider https://www.npmjs.com/package/react-shadow */
class Shadow extends React.Component {
    render() {
        return (
            <StyleSheetManager target={header}>
                <StylesProvider jss={cjss}>
                    <MuiThemeProvider theme={theme}>
                        <ThemeProvider theme={theme}>
                            <Router history={history}>
                                <Switch>
                                        <Route exact path="/in/:id/" component={FloatPanel} />
                                </Switch>
                            </Router>
                        </ThemeProvider>
                    </MuiThemeProvider>
                </StylesProvider>
            </StyleSheetManager>
        )
    }
}

ReactDOM.render(<Shadow />, root);
const retargetEvents = require("react-shadow-dom-retarget-events");
retargetEvents(shadow);
workaround.parentNode.removeChild(workaround);

/** watch for url changes and push them history */
setInterval(() => {
    const location = createBrowserHistory().location;
    if (location.pathname != history.location.pathname) {
        history.push(location.pathname);
    }
}, 1000);