import axios from 'axios';
const cheerio = require('cheerio');

export class Linkedin {

    decodeHtmlspecialChars(text) {
        var map = {
            '&amp;': '&',
            '&#038;': "&",
            '&lt;': '<',
            '&gt;': '>',
            '&quot;': '"',
            '&#039;': "'",
            '&#8217;': "’",
            '&#8216;': "‘",
            '&#8211;': "–",
            '&#8212;': "—",
            '&#8230;': "…",
            '&#8221;': '”'
        };

        return text.replace(/\&[\w\d\#]{2,5}\;/g, function(m) { return map[m]; });
    }

    getProfileByPublicIdentifier = async (linkedin) => {

        let html = await axios({method: 'get', url: `https://www.linkedin.com/in/${linkedin}`});

        const $ = cheerio.load(html.data);

        var object = {};

        $('code[id]').each((i, e) => {

            object = { urn : {}, object : {}};

            try {
                object.raw = JSON.parse(this.decodeHtmlspecialChars($(e).html()));
                object.included = object.raw.included;

            }
            catch(err) {
                return true;
            }

            if (object && object.included && object.included.length > 1) {
                var urn = null;

                //STEP1: looking if data BLOCK belongs to the profile / looking for the data
                //ENTITYURN: urn:li:fs(m)_<category>:(<profile ID>,<entry ID>)
                for(var ii in object.included) {
                    if (decodeURIComponent(object.included[ii].publicIdentifier) == decodeURIComponent(linkedin)) {
                        urn = object.included[ii].entityUrn.split(':')[3];
                    }
                }

                if(urn) {

                    //STEP2: looking if DATA in block belongs to profile, by urn profile ID in entityUrn, and forming urn list
                    for(var k in object.included) {
                        var o = object.included[k]
                        if(o.entityUrn.indexOf(urn) > -1) {
                            object.urn[o.entityUrn] = o;
                        }
                    }


                    //STEP3: creating object
                    for (var x in object.urn) {

                        //STEP3-1: creating list of list by category in entityUrn
                        //ENTITYURN: urn:li:fs_<category>:(<profile ID>,<entry ID>)
                        var category = object.urn[x]['entityUrn'].split(':')[2].split('_')[1];

                        if(!object.object[category]) {
                            object.object[category] = [];
                        }

                        object.object[category].push(object.urn[x]);


                        //STEP3-2: remove unnecessary properties
                        for (var y in object.urn[x]) {
                            if (y == '$type' || y.endsWith('Urn') || y.startsWith('*') || y == 'timePeriod' || y == 'company' || y == 'region' || !object.urn[x][y]) {
                                delete object.urn[x][y];
                            }
                        }

                        //STEP3-3: remove unnecessary categories
                        if(category.endsWith('View') || category.endsWith('Group')) {
                            delete object.object[category];
                        }

                    }

                    //STEP3-4: BUG FIX
                    if(object.object.miniProfile && object.object.miniProfile[0].publicIdentifier) {
                        object.object.profile[0].publicIdentifier = object.object.miniProfile[0].publicIdentifier;
                    }

                    if(object.object.miniProfile && object.object.miniProfile[0].picture) {
                        object.object.profile[0].picture = object.object.miniProfile[0].picture;
                    }

                    if(object.object.profile && object.object.profile[0].profilePicture) {
                        object.object.profile[0].picture = object.object.profile[0].profilePicture.displayImageReference.vectorImage;
                    }

                    if(object.object.profilePosition) {
                        object.object.position = object.object.profilePosition;
                    }

                    delete object.object['miniProfile'];

                    return false;
                }
            }

        });




        //STEP4: bulding PROFILE property
        if(object.object.profile[0].picture) {

            var tmp = {artifacts : [], rootUrl : ''};

            if (typeof object.object.profile[0].picture['com.linkedin.common.VectorImage'] !== 'undefined') {
                tmp = object.object.profile[0].picture['com.linkedin.common.VectorImage'];
            } else if (typeof object.object.profile[0].picture.artifacts !== 'undefined') {
                tmp = object.object.profile[0].picture;
            }

            object.object.profile[0].picture = null;
            for (var p in tmp.artifacts) {
                object.object.profile[0].picture = tmp.artifacts[p];
            }

            object.object.profile[0].picture = tmp.rootUrl + object.object.profile[0].picture.fileIdentifyingUrlPathSegment.replace(/&amp;/g, '&');
        } else {
            object.object.profile[0].picture = '';
        }

        var joblocality = null;
        for (var i in object.object.position) {
            joblocality = (joblocality) ? joblocality : object.object.position[i].locationName;
        }


        var profile =
        {
            firstName: object.object.profile[0].firstName,
            lastName: object.object.profile[0].lastName,
            linkedin: "https://www.linkedin.com/in/"+object.object.profile[0].publicIdentifier,
            publicIdentifier: decodeURI(object.object.profile[0].publicIdentifier),
            locality: object.object.profile[0].locationName,
            joblocality: joblocality,
            image: object.object.profile[0].picture,

            occupation: object.object.profile[0].occupation,
            geoCountryName : object.object.profile[0].geoCountryName,
            geoLocationName : object.object.profile[0].geoLocationName,
            headline : object.object.profile[0].headline,
            industryName : object.object.profile[0].industryName,
            locationName : object.object.profile[0].locationName,
            summary : object.object.profile[0].summary,
            address : object.object.profile[0].address,
            birthDate : object.object.profile[0].birthDate,
            maidenName : object.object.profile[0].maidenName,
            phoneticFirstName : object.object.profile[0].phoneticFirstName,
            phoneticLastName : object.object.profile[0].phoneticLastName,
            state : object.object.profile[0].state,
            student : object.object.profile[0].student
        }


        object.object['profile'] = [profile];

        //console.log(object.object);

        return object.object;


    }
}


export default new Linkedin();