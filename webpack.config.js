const HtmlWebpackPlugin = require('html-webpack-plugin')
//??????????
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const ExtensionReloader  = require('webpack-extension-reloader');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const postcssNormalize = require('postcss-normalize');
const GoogleFontsPlugin = require('google-fonts-plugin');
const path = require('path');
//hashes

module.exports = {
    entry: {
        background: './src/background.js',
        floatpanel: './src/floatpanel/shadow.jsx',
    },
    output: {
        filename: '[name].js',
        //filename: '[name].[hash].js'
    },
    plugins: [
        /*new CleanWebpackPlugin({cleanStaleWebpackAssets: false}),*/
        new HtmlWebpackPlugin({
            inject: true,
            title: 'asd',
            description: 'aa',
            publicPath: '/',

            filename: 'browseraction.html',
            template: './src/browseraction/browseraction.html',
            chunks: ['browseraction'],
            /*minify: {
                collapseWhitespace: true,
                removeComments: true,
                removeRedundantAttributes: true,
                removeScriptTypeAttributes: true,
                removeStyleLinkTypeAttributes: true,
                useShortDoctype: true
            }*/
        }),
        new CopyWebpackPlugin([
            { from: "./manifest.json" },
            { from: "./src/browseraction/assets/st_transparent_16.png" },
            { from: "./src/browseraction/assets/st_transparent_48.png" },
            { from: "./src/browseraction/assets/st_transparent_128.png" },
        ]),
        /*new ExtensionReloader({
            entries: {
                contentScript: ['floatpanel'],
            }
        })*/
    ],
    devtool: "source-map",
    resolve: {
        extensions: [".js", ".jsx"]
    },
    module: {
        rules: [
            {
                test: /\.js(x?)$/,
                include: [path.resolve(__dirname, "src")],
                use: [
                    { loader: 'babel-loader', options: { presets: ['@babel/env','@babel/preset-react'], plugins: [
                        "@babel/plugin-proposal-class-properties",
                        "@babel/plugin-proposal-export-default-from",
                        "@babel/plugin-transform-runtime"
                    ]}}
                ]
            },
            {
                test: /\.css$/,
                include: [
                    path.resolve(__dirname, "src"),
                    path.resolve(__dirname, "node_modules/typeface-roboto")
                ],
                use: [
                    {loader: "style-loader"},
                    {loader: "css-loader"},
                    { loader: 'postcss-loader', options: { ident: 'postcss', plugins: () => [ postcssNormalize()] } }
                ]
            },
            {
                enforce: "pre",
                test: /\.js$/,
                include: [
                    path.resolve(__dirname, "src")
                ],
                loader: "source-map-loader"
            },
            {
                test: /\.(png)|(jpg)|(woff)|(woff2)$/,
                include: [
                    path.resolve(__dirname, "src"),
                    path.resolve(__dirname, "node_modules/typeface-roboto")
                ],
                use: [
                    {loader: 'file-loader',options: {name: "[name].[ext]", esModule: false}}
                ]
            },
            {
              test: /\.svg$/,
              include: [path.resolve(__dirname, "src")],
              use: [
                {
                  loader: '@svgr/webpack',
                },
              ],
            }
        ]
    },
};
